const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, `Firstname is required`]
    },
    lastName: {
        type: String,
        required: [true, `Lastname is required`]
    },
    email: {
        type: String,
        required: [true, `Email is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    Enrollments: [
        {
            courseId:{
                type: String,
                required: [true,`Course Id is required`]
            },
            status:{
                type: String,
                default: "Enrolled"
            },
            enrolledOn:{
                type: Date,
                default: new Date()
            }
        }
    ]

}, {timestamps:true})


module.exports = mongoose.model('User', userSchema); 