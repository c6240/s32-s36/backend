const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = 3008;
const app = express();

const userRoutes = require('./routes/userRoutes')

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())


mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


app.use(`/api/users`, userRoutes);



app.listen(PORT, () => console.log(`Server is connected to port ${PORT}`))