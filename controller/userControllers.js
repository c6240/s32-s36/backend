
const res = require('express/lib/response');
const User = require('./../models/userModels')
const CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt');
const { createToken } = require('./../auth');
const router = require('../routes/userRoutes');
const { findOneAndUpdate } = require('./../models/userModels');



module.exports.register = async (reqBody) => {
    console.log(reqBody)
    const {firstName, lastName, email, password} = reqBody;

    const newUser =    new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
    })

    return await  newUser.save().then(result => {
        if (result){
            return true
        } else {
            if(result === null){
                return false
            }
        }
    })
}

module.exports.getAllUsers = async () => {
    return await User.find().then (result => result)
}

module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}


module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}

module.exports.profile = async (id) => {

    return await User.findById(id).then(result => {
        if(result){
            return result
        } else {
            if(result == null){
                return {message: `user does not exist`}
            } else {
                return err
            }
        }
    })
}

module.exports.update = async (userId, reqBody) => {
	const userdata = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password:CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}
	return await User.findByIdAndUpdate(userId, {$set: userdata}, {new:true}).then((result, err) => {
		if (result){
			result.password = "****"
			return result
		} else {
			return err
		}
	})
}

module.exports.updatepw = async (userId, reqBody) => {
	const userdata = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password:CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}
	return await User.findByIdAndUpdate(userId, {$set: userdata.password}, {new:true}).then((result, err) => {
		if (result){
			result.password = "****"
			return result
		} else {
			return err
		}
	})
}

module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody


	return await User.findOneAndUpdate({email: email}, {$set:{isAdmin:true}}).then((result, err) => result ? true : err )
}

module.exports.userStatus = async (reqBody) =>{
	return await User.findOneAndUpdate({email: reqBody.email}, {$set:{isAdmin:false}}).then((result, err) => result ? true : err )
}

module.exports.deleteUser = async (reqBody) =>{
	return await User.findOneAndDelete({email: reqBody.email}).then((result, err) => result ? true : err )
}



