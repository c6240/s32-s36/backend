const express = require('express');
const router = express.Router();
const {verify, decode, verifyAdmin} = require('./../auth.js')
const {register, getAllUsers,checkEmail,login,profile,update,updatepw,adminStatus,userStatus,deleteUser} = require('./../controller/userControllers')

router.get('/', async (req, res) => {

    try {
        await getAllUsers().then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }


})

router.post(`/register`, async (req, res) => {
    // console.log(req.body)

    try {
        await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }



})

router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})

router.post('/login', (req,res) => {
    // console.log(req.body)
    try{    
        login(req.body).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})


router.get('/profile', verifyAdmin, async (req, res) => {
    // res.send(`welcome to get request`)
    const userId = decode(req.headers.authorization).id
    // console.log(userId)
    try{
        profile(userId).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

router.put('/update',verify, async (req, res) => {
    const userId = decode(req.headers.authorization).id
//    console.log(userId)
   
    try{
		await updatepw(userId,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/update-password',verify, async (req, res) => {
    const userId = decode(req.headers.authorization).id
//    console.log(userId)
   
    try{
		await update(userId,req.body.password).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// router.patch('/isAdmin', verify, async (req,res) => {
//     // console.log(req.body)
//     // console.log(decode(req.headers.authorization).isAdmin)
//     const admin = decode(req.headers.authorization).isAdmin


//     try{
//         if(admin) {
//            await adminStatus(req.body).then(result => res.send(result)) 
//         }else {
//             res.send(`You are not authorized`)
//         }
//     } catch(err){
//         res.status(500).json(err)
//     }
// })


router.patch('/isAdmin', verifyAdmin, async (req,res) => {
    try{
           await adminStatus(req.body).then(result => res.send(result)) 
    } catch(err){
        res.status(500).json(err)
    }
})

router.patch('/isUser', verifyAdmin, async (req, res) => {
    try{
        await userStatus(req.body).then( result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

router.delete('/delete-user', verifyAdmin, async (req, res) => {
    try{
        await deleteUser(req.body).then( result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})




module.exports = router;